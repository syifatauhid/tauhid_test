import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { StudentDataComponent } from './student-data/student-data.component';
import { ProfileComponent } from './profile/profile.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [
  {path : '', redirectTo: '/student', pathMatch:'full'},
  {path : 'student', component: StudentDataComponent},
  {path : 'profile', component: ProfileComponent},
  {path : '**', component: PageNotFoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
export const routingComponents = [StudentDataComponent,ProfileComponent];
