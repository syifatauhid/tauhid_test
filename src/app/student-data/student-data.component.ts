import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-student-data',
  templateUrl: './student-data.component.html',
  styles: []
})
export class StudentDataComponent implements OnInit {
public data = [
  {'id':1,'name':'Muhammad Syifa Tauhid','NIM':201743500313},
  {'id':2,'name':'Aji Ibra Maulana','NIM':201743500306},
  {'id':3,'name':'Muhammad Fahrul Fadli','NIM':201743500309},
  {'id':4,'name':'Teri Almagreza','NIM':201743500325},
  {'id':5,'name':'Rizki Nur Amartianto','NIM':201743500318}
];
  constructor() { }

  ngOnInit(): void {
  }

}
